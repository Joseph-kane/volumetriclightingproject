Created in Unity 2017.1.0f3 Personal edition in DX11
Upgraded to Unity 2018.1.6 Personal edition

Lower calculation resolution had to be disabled last minute because of a flicker bug. Can be reenabled by uncommenting lines 117 and 133 in "VolLighting_Camera" script. The issue continues to occur even when editor is paused.


The scripts which can be added to any project to add volumetric lighting, are contained withing the "Scripts" and "Shaders" folders. All that is required is for a project to have these folders and for the VolLight script to 
be attached to any light that you wish to be volumetric and for VolCamera to be attached to any camera you wish to render the volumetric lights.

Examples of volumetric lighting working are contained in the "Scenes" folder