﻿using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Light))]
public class VolLighting_Light : MonoBehaviour
{
    //Enum
    public enum CalculationResolution{Full = 1, Half = 2, Quarter = 4}

    //Inspector Variables
    [Tooltip("Spotlight ONLY. Number of sides of the spotlight geometry")]
    public int sides = 16;
    [Tooltip("Point light ONLY. Number of vertical loops in the direction of the X axis, for the point lights geometry")]
    public int XLoops = 16;
    [Tooltip("Point light ONLY. Number of horizontal loops in the direction of the Y axis, for the point lights geometry")]
    public int YLoops = 8;

    //Shader variables
    [Range(-0.99f, 0.99f)]
    public float anisotropicFactor;
    public int raymarchSteps;
    public float maxRaymarchLength = 1000;
    public CalculationResolution calculationResolution = CalculationResolution.Full;
    public float extinctionCoefficient;
    public float scatteringCoefficient;
    [Tooltip("Multiplies depth value by bias, to account for inaccuracys in depth texture")]
    public float depthBias = 0.1f;

    //Component references
    private Light lightComponent;
    //private MeshFilter meshFilter; //Unused after change to rendering with command buffer's draw mesh function
    //private MeshRenderer meshRenderer; //Unused after change to rendering with command buffer's draw mesh function
    static MeshCreator meshCreator = new MeshCreator();

    static Shader volLightShader;
    private Material volLightMaterial;

    private CommandBuffer afterShadowmapCommandBuffer;

    //Shader property IDs for faster use of reused property values
    private int _cameraInsideLightVolume_PropertyID;

    public Texture samplingOffsetTexture; //8x8 sampling offset texture for interleaved sampling
    
    [Tooltip("Noise texture to simulate particles in the air")]
    public Texture particleNoise;
    [Tooltip("Speed of moving particles")]
    public float particleNoiseSpeed = 1;
    [Tooltip("Direction of moving particles")]
    public Vector3 particleNoiseDirection = new Vector3(0,-1, 0);


    void Awake ()
	{
        volLightShader = Shader.Find("VolumetricLighting/VolLight");
        volLightMaterial = new Material(volLightShader);
        lightComponent = GetComponent<Light>();

        if (lightComponent.type == LightType.Point)
        {
            //Enable keywords
            volLightMaterial.EnableKeyword("POINT");
            volLightMaterial.EnableKeyword("SHADOWS_CUBE");
            volLightMaterial.EnableKeyword("SHADOWS_NATIVE");

            //Disable keywords
            volLightMaterial.DisableKeyword("SPOT");
            volLightMaterial.DisableKeyword("SHADOWS_DEPTH");
            volLightMaterial.DisableKeyword("DIRECTIONAL");
        }
        else if (lightComponent.type == LightType.Spot)
        {
            //Enable keywords
            volLightMaterial.EnableKeyword("SPOT");
            volLightMaterial.EnableKeyword("SHADOWS_DEPTH");
            volLightMaterial.EnableKeyword("SHADOWS_NATIVE");

            //Disable keywords
            volLightMaterial.DisableKeyword("POINT");
            volLightMaterial.DisableKeyword("SHADOWS_CUBE");
            volLightMaterial.DisableKeyword("DIRECTIONAL");
        }
        else if(lightComponent.type == LightType.Directional)
        {
            //Enable keywords
            volLightMaterial.EnableKeyword("DIRECTIONAL");
            volLightMaterial.EnableKeyword("SHADOWS_SCREEN");
            volLightMaterial.EnableKeyword("SHADOWS_NATIVE");

            //Disable keywords
            volLightMaterial.DisableKeyword("POINT");
            volLightMaterial.DisableKeyword("SHADOWS_CUBE");
            volLightMaterial.DisableKeyword("SPOT");
        }

        //Setup a command buffer for light
        if (afterShadowmapCommandBuffer == null)
        {
            afterShadowmapCommandBuffer = new CommandBuffer();
            afterShadowmapCommandBuffer.name = "After shadow map command buffer";
        }
        
        lightComponent.AddCommandBuffer(LightEvent.AfterShadowMap, afterShadowmapCommandBuffer);

        //Set property IDs of shader propertys
        _cameraInsideLightVolume_PropertyID = Shader.PropertyToID("_cameraInsideLightVolume");
    }

	void Update ()
	{
        //Set lights position in shader
        volLightMaterial.SetVector("_lightSourcePos", transform.position); 

    }

    void OnEnable()
    {
        Camera.onPreRender += VolumetricLightPreRender;
    }

    void OnDisable()
    {
        Camera.onPreRender -= VolumetricLightPreRender;
    }

    //Runs for each camera before that camera renders
    void VolumetricLightPreRender(Camera renderingCamera)
    {
        Camera cameraComponent = renderingCamera.GetComponent<Camera>();
        VolLighting_Camera volLighting_Camera_Script = renderingCamera.GetComponent<VolLighting_Camera>();

        if (volLighting_Camera_Script != null)
        {
            volLightMaterial.SetInt(_cameraInsideLightVolume_PropertyID, CameraInsideLightVolume(renderingCamera.transform.position) ? 1 : 0);
            
            SetShaderVariables();

            //Create Model view matrix - InBuilt shader MVP seems to use shadowmap virtual camera during shadowmap stage
            Matrix4x4 projectionMatrix = GL.GetGPUProjectionMatrix(Matrix4x4.Perspective(cameraComponent.fieldOfView, cameraComponent.aspect, cameraComponent.nearClipPlane, cameraComponent.farClipPlane), true);
            // Matrix4x4 viewProjectionMatrix = projectionMatrix * cameraComponent.worldToCameraMatrix;
            Matrix4x4 modelMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Matrix4x4 mvpMatrix = (projectionMatrix * cameraComponent.worldToCameraMatrix) * modelMatrix;
            volLightMaterial.SetMatrix("_mvpMatrix", mvpMatrix);


            afterShadowmapCommandBuffer.Clear();
            afterShadowmapCommandBuffer.SetGlobalTexture("_ShadowMapTexture", BuiltinRenderTextureType.CurrentActive);
            afterShadowmapCommandBuffer.SetRenderTarget(volLighting_Camera_Script.GetVolLightBuffer(calculationResolution)); //Set to render result to the volLightBuffer

            if (lightComponent.type == LightType.Point)
            {
                afterShadowmapCommandBuffer.DrawMesh(meshCreator.createSphere(lightComponent.range, XLoops, YLoops), transform.localToWorldMatrix, volLightMaterial);
            }
            else if (lightComponent.type == LightType.Spot)
            {
                //Radius calculated knowing that "tan(angle) = Opposite/Adjacent" with opposite being the radius and adjacent being the range
                float radius = SpotlightRadius();
                afterShadowmapCommandBuffer.DrawMesh(meshCreator.createCone(lightComponent.range, radius, sides), transform.localToWorldMatrix, volLightMaterial);
            }
        }
    }


    /// <summary>
    /// Sets shader variables
    /// </summary>
    private void SetShaderVariables()
    {
        //Assign variables
        volLightMaterial.SetFloat("_scatteringCoefficient", scatteringCoefficient);
        volLightMaterial.SetFloat("_extinctionCoefficient", extinctionCoefficient);
        volLightMaterial.SetInt("_raymarchSteps", raymarchSteps);
        volLightMaterial.SetFloat("_maxRaymarchLength", maxRaymarchLength);
        volLightMaterial.SetFloat("_anisotropicFactor", anisotropicFactor);
        volLightMaterial.SetFloat("_depthBias", depthBias);

        volLightMaterial.SetVector("_lightSourcePos", transform.position); //Set light position
        volLightMaterial.SetColor("_lightColour", lightComponent.color); //Set light colour
        volLightMaterial.SetVector("_lightSourceDirection", transform.forward); //Set light position

        volLightMaterial.SetTexture("_samplingOffsetTexture", samplingOffsetTexture);

        volLightMaterial.SetInt("_calculationResolution", (int)calculationResolution);

        volLightMaterial.SetTexture("_particleNoise", particleNoise);
        volLightMaterial.SetFloat("_particleNoiseSpeed", particleNoiseSpeed);
        volLightMaterial.SetVector("_particleNoiseDirection", particleNoiseDirection.normalized);



        if (lightComponent.type == LightType.Spot)
        {
            volLightMaterial.SetFloat("_lightSourceRange", lightComponent.range);
            volLightMaterial.SetFloat("_lightSourceRadius", SpotlightRadius());
        }

    }

    //TODO Might need to check camera near clip
    //Returns true if the camera is inside the lights volume
    private bool CameraInsideLightVolume(Vector3 cameraPos)
    {
        if (lightComponent.type == LightType.Point) //Point light - Sphere
            return Vector3.Distance(transform.position, cameraPos) < lightComponent.range; //Returns True if distance from light to camera is less than radius
        else if (lightComponent.type == LightType.Spot) //Spotlight - Cone
        {
            //return true; TODO Test this
            //True if angle from light vector to camera is less than light vector to light edge AND length of lightsource to camera vector projected onto light vector, is less than light range
            Vector3 lightToCameraVector = cameraPos - transform.position;
            return Vector3.Angle(lightToCameraVector, transform.forward) < lightComponent.spotAngle / 2 && Vector3.Dot(lightToCameraVector, transform.forward) < lightComponent.range;
        }
        else //TODO Remove and replace above else if with just else
        {
            Debug.LogError("Wrong light type for volumetric lighting. Direction lights dont work");
            return false;
        }
    }


    /// <summary> Calculates the radius of the base of a spotlight </summary>
    /// <returns> The radius of the base of this spotlight </returns>
    private float SpotlightRadius()
    {
        return Mathf.Tan(Mathf.Deg2Rad * lightComponent.spotAngle / 2) * lightComponent.range;
    }
    

    //////////////////////____________ OLD CODE ____________///////////////////////////////////////////
    
    /* Old code used for rendering meshes for point and spotlights. 
     * REMOVED REASON: The code is no longer in use as the rendering of the meshes no longer use unity components and instead render using a command buffer that renders directly to a RenderTexture after shadowmap creation
     * 
    /// <summary>
    /// Updates the lights volume with a new mesh. Can be recalled to recreate the lights volume in cases where variables are changed for the light
    /// </summary>
    public void UpdateLightVolume()
    {
        if (lightComponent.type == LightType.Point)
            CreatePointLightVol();
        else if (lightComponent.type == LightType.Spot)
            CreateSpotLightVol();
    }

    /// <summary>
    /// Creates a sphere the size of the point light to represent the lights volume
    /// </summary>
    private void CreatePointLightVol()
    {
        meshFilter.mesh = meshCreator.createSphere(lightComponent.range, XLoops, YLoops);
        //Assign material
        meshRenderer.material = volLightMaterial;
        SetShaderVariables();
    }

    /// <summary>
    /// Creates a cone the size of the spotlight to represent the lights volume
    /// </summary>
    private void CreateSpotLightVol()
    {
        //Radius calculated knowing that "tan(angle) = Opposite/Adjacent" with opposite being the radius and adjacent being the range
        float radius = Mathf.Tan(Mathf.Deg2Rad * lightComponent.spotAngle / 2) * lightComponent.range;
        meshFilter.mesh = meshCreator.createCone(lightComponent.range, radius, sides);
        //Assign material
        meshRenderer.material = volLightMaterial;
        SetShaderVariables();
    }
    */
}
