﻿using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(Camera))]
public class VolLighting_Camera : MonoBehaviour
{
    public bool blurEnabledNormalRes = true;
    public bool blurEnabledHalfRes = false;
    public bool blurEnabledQuarter = false;

    private Camera cameraComponent;

    //VolLightBuffer - Render texture that hold the results of volumetric lighting calculations
    private RenderTexture volLightBuffer; //The full screen resolution volLightBuffer
    private RenderTexture volLightBufferHalfRes; //Half size version of volLightBuffer
    private RenderTexture volLightBufferQuarterRes; //Half size version of volLightBuffer

    //Shader that combines volumetric lighting with the current camera render
    public Shader blitToTexture;
    public Material blitToTextureMaterial;

    public Shader depthDownscaleShader;
    public Material depthDownscaleMaterial;

    public Shader bilateralUpsampleShader;
    public Material bilateralUpsampleMaterial;

    public Shader blurShader;
    public Material blurMaterial;

    private CommandBuffer depthDownscaleCommandBuffer;

    public CommandBuffer volLightClearCommandBuffer;

    //Downscaled Depth Texture
    private RenderTexture cameraDepthTextureHalfRes; //Half size version of this camera depth texture
    private RenderTexture cameraDepthTextureQuarterRes; //Quarter size version of this camera depth texture




    void Awake ()
	{
        cameraComponent = GetComponent<Camera>();

        CreateVolLightBuffers();

        //Turn on camera depth texture rendering
        cameraComponent.depthTextureMode = DepthTextureMode.Depth;

        if (blitToTexture == null)
            blitToTexture = Shader.Find("VolumetricLighting/BlitToTexture"); //Shader used for adding the volumetric light buffer to the current screen and doing a "Blit" of the result to a rendertexture
        blitToTextureMaterial = new Material(blitToTexture);

        if(depthDownscaleShader == null)
            depthDownscaleShader = Shader.Find("VolumetricLighting/DepthDownscale");
        depthDownscaleMaterial = new Material(depthDownscaleShader);

        if (bilateralUpsampleShader == null)
            bilateralUpsampleShader = Shader.Find("VolumetricLighting/BilateralUpsample");
        bilateralUpsampleMaterial = new Material(bilateralUpsampleShader);

        if (blurShader == null)
            blurShader = Shader.Find("VolumetricLighting/Blur");
        blurMaterial = new Material(blurShader);


        volLightClearCommandBuffer = new CommandBuffer();
        volLightClearCommandBuffer.name = "Volumetric light clearing command buffer";
        cameraComponent.AddCommandBuffer(CameraEvent.BeforeGBuffer, volLightClearCommandBuffer);
        volLightClearCommandBuffer.SetRenderTarget(volLightBufferQuarterRes);
        volLightClearCommandBuffer.ClearRenderTarget(false, true, new Color(0, 0, 0, 0));
        volLightClearCommandBuffer.SetRenderTarget(volLightBufferHalfRes);
        volLightClearCommandBuffer.ClearRenderTarget(false, true, new Color(0, 0, 0, 0));
        volLightClearCommandBuffer.SetRenderTarget(volLightBuffer);
        volLightClearCommandBuffer.ClearRenderTarget(false, true, new Color(0, 0, 0, 0));

        //Command buffer for downscaling depth buffer
        depthDownscaleCommandBuffer = new CommandBuffer();
        depthDownscaleCommandBuffer.name = "Depth downscaling command buffer";
        cameraComponent.AddCommandBuffer(CameraEvent.BeforeLighting, depthDownscaleCommandBuffer); //After depth texture camera event doesnt render in deffered
        CreateDownscaledDepthTextures();
    }

    private void OnDisable()
    {
        cameraComponent.depthTextureMode = DepthTextureMode.None; //Performance fix
        
        //Release rendertexture resources
        volLightBuffer.Release();
        volLightBufferHalfRes.Release();
        volLightBufferQuarterRes.Release();
    }


    [ImageEffectOpaque]
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        
        if (blurEnabledQuarter)
        {
            RenderTexture temp1 = RenderTexture.GetTemporary(volLightBufferQuarterRes.width, volLightBufferQuarterRes.height, 0, RenderTextureFormat.ARGBHalf);
            temp1.filterMode = FilterMode.Bilinear;
            Graphics.Blit(volLightBufferQuarterRes, temp1, blurMaterial, 4);
            Graphics.Blit(temp1, volLightBufferQuarterRes, blurMaterial, 5);

            RenderTexture.ReleaseTemporary(temp1);
        }


        //Quarter res -> Half res
//LOWER RENDER RESOLUTION SOMETIMES CAUSING BAD FLICKER
//Disabled last minute because of this new found bug that isnt in documentation
        //Graphics.Blit(volLightBufferQuarterRes, volLightBufferHalfRes, bilateralUpsampleMaterial, 1);


        if (blurEnabledHalfRes)
        {
            RenderTexture temp2 = RenderTexture.GetTemporary(volLightBufferHalfRes.width, volLightBufferHalfRes.height, 0, RenderTextureFormat.ARGBHalf);
            temp2.filterMode = FilterMode.Bilinear;
            Graphics.Blit(volLightBufferHalfRes, temp2, blurMaterial, 2);
            Graphics.Blit(temp2, volLightBufferHalfRes, blurMaterial, 3);

            RenderTexture.ReleaseTemporary(temp2);
        }

        //Half res -> Full res
//LOWER RENDER RESOLUTION SOMETIMES CAUSING BAD FLICKER
//Disabled last minute because of this new found bug that isnt in documentation
        //Graphics.Blit(volLightBufferHalfRes, volLightBuffer, bilateralUpsampleMaterial, 0);

         
        if (blurEnabledNormalRes)
        {
            RenderTexture temp3 = RenderTexture.GetTemporary(volLightBuffer.width, volLightBuffer.height, 0, RenderTextureFormat.ARGBHalf);
            temp3.filterMode = FilterMode.Bilinear;
            Graphics.Blit(volLightBuffer, temp3, blurMaterial, 0);
            Graphics.Blit(temp3, volLightBuffer, blurMaterial, 1);

            RenderTexture.ReleaseTemporary(temp3);
        }


        blitToTextureMaterial.SetTexture("_VolLight", volLightBuffer);
        Graphics.Blit(source, destination, blitToTextureMaterial);

        volLightBuffer.DiscardContents();
    }

    /// <summary>
    /// Returns the volumetric light buffer corresponding to the given resolution.
    /// </summary>
    /// <param name="resolution"> Resolution of buffer to return </param>
    /// <returns>The volumetric light buffer corresponding to the given resolution</returns>
    public RenderTexture GetVolLightBuffer(VolLighting_Light.CalculationResolution resolution)
    {
        switch (resolution)
        {
            case VolLighting_Light.CalculationResolution.Full:
                return volLightBuffer;

            case VolLighting_Light.CalculationResolution.Half:
                return volLightBufferHalfRes;

            case VolLighting_Light.CalculationResolution.Quarter:
                return volLightBufferQuarterRes;

            default:
                Debug.Log("GetVolLightBuffer() has invalid input resolution. Defaulting to full resolution");
                return volLightBuffer;
        }
    }

    public void CreateVolLightBuffers()
    {
        volLightBuffer = new RenderTexture(cameraComponent.pixelWidth, cameraComponent.pixelHeight, 0, RenderTextureFormat.ARGBFloat);
        volLightBufferHalfRes = new RenderTexture(cameraComponent.pixelWidth/2, cameraComponent.pixelHeight/2, 0, RenderTextureFormat.ARGBFloat);
        volLightBufferQuarterRes = new RenderTexture(cameraComponent.pixelWidth/4, cameraComponent.pixelHeight/4, 0, RenderTextureFormat.ARGBFloat);
    }

    public void CreateDownscaledDepthTextures()
    {
        depthDownscaleCommandBuffer.Clear(); //Removes all commands currently in the buffer

        cameraDepthTextureHalfRes = new RenderTexture(cameraComponent.pixelWidth/2, cameraComponent.pixelHeight/2, 0, RenderTextureFormat.RFloat);
        cameraDepthTextureQuarterRes = new RenderTexture(cameraComponent.pixelWidth/4, cameraComponent.pixelHeight/4, 0, RenderTextureFormat.RFloat);

        depthDownscaleCommandBuffer.Blit(null, cameraDepthTextureHalfRes, depthDownscaleMaterial, 0); 
        depthDownscaleCommandBuffer.SetGlobalTexture("_CameraDepthTextureHalfRes", cameraDepthTextureHalfRes);
        depthDownscaleCommandBuffer.Blit(null, cameraDepthTextureQuarterRes, depthDownscaleMaterial, 1);
        depthDownscaleCommandBuffer.SetGlobalTexture("_CameraDepthTextureQuarterRes", cameraDepthTextureQuarterRes); 
    }
}
