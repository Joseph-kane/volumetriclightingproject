﻿using UnityEngine;

public class MeshCreator
{
    public Mesh createSphere(float radius, int XLoops, int YLoops)
    {
        Mesh sphereMesh = new Mesh();
        Vector3[] vertices = new Vector3[(XLoops * YLoops) + 2];
        int[] triangles = new int[((XLoops * 2) + (XLoops * (YLoops-1) * 2)) * 3]; //Amount of triangles around poles + quads on all other loops * 2(to convert quads to triangles)
        int nextVertIndex = 1; //Next position in vert array to insert
        int nextTriIndex = 0; //Next position in triangles array to insert

        ///////////////////////////// Construct mesh ////////////////////////////////////////

        ////////////// Set Vertices /////////////////////////
        //Create pole vertices
        vertices[0] = new Vector3(0, radius, 0); //Top pole vertex
        vertices[vertices.Length-1] = new Vector3(0, -radius, 0); //Bottom pole vertex

        //Create other vertices
        for (int y = 1; y <= YLoops; y++) //Run once for each vertex loop between pole vertices
        {
            float yAngle = 180f / (YLoops + 1) * y; //Angle of next ring with rings distributed evenly downwards on sphere, between the pole vertices.
            //Debug.Log("Y: " + y + "Y angle = " + yAngle + "Calculation: " + (YLoops+1) + "* y =" + ((YLoops+1)*y));
            float vertY = radius * Mathf.Cos(yAngle * Mathf.Deg2Rad);

            for (int x = 0; x < XLoops; x++)
            {
                float angle = 360f / XLoops * x; //Angle of next point with points distributed evenly along circle loop

                float vertX = radius * Mathf.Cos(angle * Mathf.Deg2Rad) * Mathf.Sin(yAngle * Mathf.Deg2Rad);
                float vertZ = radius * Mathf.Sin(angle * Mathf.Deg2Rad) * Mathf.Sin(yAngle * Mathf.Deg2Rad);
                 
                vertices[nextVertIndex++] = new Vector3(vertX, vertY, vertZ);
                //GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
                //temp.transform.position = new Vector3(vertX, vertY, vertZ);
                //temp.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            }
        }

        //////////// Set Triangles /////////////////////////
        //Set Triangles - Top Pole
        for (int i = 1; i < XLoops; i++)
        {
            triangles[nextTriIndex++] = i; //Left vertex in loop below pole
            triangles[nextTriIndex++] = 0; //Top pole vertex
            triangles[nextTriIndex++] = i + 1; //Right vertex in loop below pole
        }
        //Last triangle joining first and last vertex to pole
        triangles[nextTriIndex++] = XLoops; //Last vertex in loop below pole
        triangles[nextTriIndex++] = 0; //Pole vertex
        triangles[nextTriIndex++] = 1; //First vertex in loop below pole


        //Set Triangles - Bottom Pole
        for (int i = 0; i < XLoops-1; i++)
        {
            triangles[nextTriIndex++] = vertices.Length - XLoops + i - 1; //Left vertex in loop above pole
            triangles[nextTriIndex++] = vertices.Length - XLoops + i; //Right vertex in loop above pole
            triangles[nextTriIndex++] = vertices.Length - 1; //Top pole vertex
            
        }
        //Last triangle joining first and last vertex to pole
        triangles[nextTriIndex++] = vertices.Length-2; //Last vertex in loop above pole
        triangles[nextTriIndex++] = vertices.Length-(XLoops+1); //First vertex in loop above pole
        triangles[nextTriIndex++] = vertices.Length - 1; //Pole vertex

        //Set Triangles - Center loops
        for (int y = 0; y < YLoops-1; y++)
        {
            for (int x = 0; x < XLoops-1; x++)
            {
                //Left triangle
                triangles[nextTriIndex++] = (y * XLoops) + x + 1; 
                triangles[nextTriIndex++] = (y * XLoops) + x + 2; 
                triangles[nextTriIndex++] = ((y + 1) * XLoops) + x + 1;
                
                //Right triangle
                triangles[nextTriIndex++] = ((y + 1) * XLoops) + x + 2;
                triangles[nextTriIndex++] = ((y + 1) * XLoops) + x + 1;
                triangles[nextTriIndex++] = (y * XLoops) + x + 2;
            }
            //Last quad joining first and last vertex to pole
            triangles[nextTriIndex++] = (y * XLoops) + XLoops;
            triangles[nextTriIndex++] = (y * XLoops) + 1;
            triangles[nextTriIndex++] = ((y + 1) * XLoops) + XLoops;
            
            triangles[nextTriIndex++] = ((y + 1) * XLoops) + 1;
            triangles[nextTriIndex++] = ((y + 1) * XLoops) + XLoops;
            triangles[nextTriIndex++] = (y * XLoops) + 1;
        }

        //Debug.Log("Should be, Total vertices:" + vertices.Length);
        //Debug.Log("Should be, Total triangles:" + triangles.Length/3);
        //Debug.Log("Actual, Total vertices:" + (nextVertIndex+1));
        //Debug.Log("Actual, Total triangles:" + nextTriIndex/3);

        //Add vertices and triangles to mesh
        sphereMesh.vertices = vertices;
        sphereMesh.triangles = triangles;
        return sphereMesh; //Returns created cone mesh
    }

    /// <summary> Creates a cone mesh without normals or UV </summary>
    /// <returns> Cone mesh with no normals or UV</returns>
    public Mesh createCone(float height, float radius, int sides)
    {
        Mesh coneMesh = new Mesh();
        Vector3[] vertices = new Vector3[sides + 1];
        int[] triangles = new int[(sides + ((sides / 2) - 1) * 2) * 3]; //Amount of triangles on sides + quads on cap * by 2(to get number of triangles on cap) and all * by 3(for the 3 vertices that make up each triangle)

        ///////////////////////////// Construct mesh ////////////////////////////////////////

        ////////////// Set Vertices /////////////////////////
        //Create top point vertex
        vertices[0] = new Vector3(0,0,0);

        //Create other vertices
        for (int i = 0; i < sides; i++)
        {
            float angle = 360 / sides * i; //Angle of next point with points distributed evenly along circle cap

            float x = radius * Mathf.Cos(angle * Mathf.Deg2Rad);
            float y = radius * Mathf.Sin(angle * Mathf.Deg2Rad);

            vertices[i+1] = new Vector3(x, y, height);
        }
        
        //////////// Set Triangles /////////////////////////
        //Set Triangles - Sides
        for (int i = 0; i < sides-1; i++)
        {
            int triCount = i * 3; //The triangle the loop is on

            triangles[triCount] = i+1; //Left vertex at Cap
            triangles[triCount + 1] = 0; //Top point vertex
            triangles[triCount + 2] = i+2; //Right vertex at Cap
        }
        //Last triangle joining first and last vertex
        triangles[(sides-1)*3] = sides; //Left vertex at Cap
        triangles[(sides-1)*3 + 1] = 0; //Top point vertex
        triangles[(sides-1)*3 + 2] = 1; //Right vertex at Cap
        
        //Set Triangles - Cap
        for (int i = 0; i < ((sides / 2) - 1); i++) //Number of loops is = quads on cap
        {
            int quadSet = (sides*3) + (i * 6); //The quad the loop is on

            //First triangle of quad
            triangles[quadSet] = sides - i;
            triangles[quadSet + 1] = i + 1;
            triangles[quadSet + 2] = sides - 1 - i; 
            //Second triangle of quad
            triangles[quadSet + 3] = i + 1;
            triangles[quadSet + 4] = i + 2;
            triangles[quadSet + 5] = sides -1 - i;
        }
        
        //Add vertices and triangles to mesh
        coneMesh.vertices = vertices;
        coneMesh.triangles = triangles;
        return coneMesh; //Returns created cone mesh
    }
}
