﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public float moveSpeed = 1;

    public KeyCode walkForward = KeyCode.W;
    public KeyCode walkBackward = KeyCode.S;
    public KeyCode walkLeft = KeyCode.A;
    public KeyCode walkRight = KeyCode.D;

    public CharacterController charController;

	void Update ()
	{
		if(Input.GetKey(walkForward))
        {
            charController.SimpleMove(transform.forward * moveSpeed);
        }
        else if (Input.GetKey(walkBackward))
        {
            charController.SimpleMove(-transform.forward * moveSpeed);
        }

        if(Input.GetKey(walkLeft))
        {
            charController.SimpleMove(-transform.right * moveSpeed);
        }
        else if (Input.GetKey(walkRight))
        {
            charController.SimpleMove(transform.right * moveSpeed);
        }
    }
}
