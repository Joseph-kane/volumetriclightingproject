﻿Shader "VolumetricLighting/VolLight"
{
	SubShader
	{
		Pass
		{
			Tags{"RenderType"="Opaque" "LightMode"="Deferred"}

			ZWrite Off
			ZTest Always
			Blend One One
			Cull Front //Render only backfaces

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.0
			//#pragma shader_feature POINT
			//#pragma shader_feature SHADOWS_CUBE
			//#pragma shader_feature SPOT
			//#pragma shader_feature SHADOWS_DEPTH
			#pragma multi_compile POINT SPOT DIRECTIONAL

#if defined(POINT)
			#pragma shader_feature SHADOWS_CUBE
#else//SPOT
			#pragma shader_feature SHADOWS_DEPTH
#endif

			#define PI 3.1415926535897932384626433832795
			#define SHADOWS_NATIVE

			#include "UnityCG.cginc"
			#include "UnityDeferredLibrary.cginc"
			//#include "UnityShadowLibrary.cginc"
			//#include "AutoLight.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 screenPos : TEXCOORD0;
				float3 worldPos : TEXCOORD1;
				float3 viewDir : TEXCOORD2;
			};

			int _raymarchSteps;
			float _scatteringCoefficient;
			float _extinctionCoefficient;
			float _anisotropicFactor;
			float _maxRaymarchLength;
			float _depthBias; //Used to slightly bias depth results to make up for small inaccuracys in depth

			float3 _lightSourcePos;
			float3 _lightColour;
			float3 _lightSourceDirection;

#if defined(SPOT)
			float _lightSourceRange;
			float _lightSourceRadius;
#endif

			int _cameraInsideLightVolume; //Calculated in the C# script

			int _calculationResolution; // 1 = Full  	2 = Half   4 = Quarter
			sampler2D _CameraDepthTextureHalfRes; //Half resolution depth texture
			sampler2D _CameraDepthTextureQuarterRes; //Quarter resolution depth texture

			//New MVP - MVP ends up wrong during shadowmap rendering as it uses virtual camera of shadowmap
			float4x4 _mvpMatrix;

			sampler2D _samplingOffsetTexture; //Texture for interleaved sampling
			float4 _samplingOffsetTexture_TexelSize;

			//Particle noise
			sampler2D _particleNoise;
			int _particleNoiseSpeed;
			float3 _particleNoiseDirection;



			v2f vert (appdata v)
			{
				v2f o;

				//o.vertex = UnityObjectToClipPos(v.vertex); //Uses standard MVP
				o.vertex = mul(_mvpMatrix, v.vertex);
				o.screenPos = ComputeScreenPos(o.vertex); //Screen position converted from clip space
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				//o.worldPos.w = distance(_WorldSpaceCameraPos, o.worldPos.xyz); //Removed as caused slight errors that threw calculations off
				o.viewDir = -normalize(WorldSpaceViewDir(v.vertex));

				return o;
			}

			//Approximates the distribution of light scattering using the Henyey-Greenstein phase function
			float PhaseFucntion(float g, float3 viewVector, float3 lightVector)
			{
				//Needs extra precalculations through c# to reduce per pixel and per frame calculations
				return (1 / (4 * PI)) * ((1 - pow(g,2)) / (pow(1 + pow(g,2) - 2*g*dot(viewVector, lightVector), 1.5)));
			}

			//Calculates the light contribution at the inputted position
			float CalculateLightContribution(float3 calcPos, float3 rayDirection)
			{
				float3 lightDirection = normalize(calcPos - _lightSourcePos); //Direction light travels from source
				return PhaseFucntion(_anisotropicFactor, rayDirection, lightDirection) * _scatteringCoefficient;
			}

			//Calculates the amount of light that passes through the light volume
			float CalculateLightTransmittance(float3 stepSize)
			{
				return exp(-_extinctionCoefficient * stepSize);
			}


			//Returns 1 if visible.
			bool CheckVisibleFromLight(float3 calcPos)
			{

#if defined(POINT)
				float3 shadowCoord = calcPos - _lightSourcePos;
#else //SPOT
				float3 shadowCoord = calcPos;
#endif


				return UnityDeferredSampleRealtimeShadow(0, shadowCoord, float2(0,0));

				//return UnityDeferredSampleRealtimeShadow(0, mul(float4(shadowCoord,1),_mvMatrix), float2(0,0));

			}

			//Returns the colour of the volumetric lighting for this pixel
			float4 Raymarch(float3 startPosition, float3 rayDirection, float distance, float2 screenPos)
			{
				float volLight = 0;
				float transmittance = 1;

				float stepSize = distance / _raymarchSteps;

				//Interleaved sampling
				float2 sampleTextureSize = _samplingOffsetTexture_TexelSize.zw; //Size of sample texture
				float2 sampleTexPixel = fmod(floor(screenPos*_ScreenParams.xy), sampleTextureSize); //Pixel on sampling offset texture to use
				float offset = tex2D(_samplingOffsetTexture, (sampleTexPixel + float2(0.5, 0.5)) / sampleTextureSize).a * stepSize;

				//Set start position
				float3 raymarchPos = startPosition + (rayDirection*offset);

				[loop] //Needed to stop loop unroll error
				for(int i = 0; i <= _raymarchSteps; i++)
				{
					if(CheckVisibleFromLight(raymarchPos))
					{
						volLight += CalculateLightContribution(raymarchPos, rayDirection) * stepSize * (tex2D(_particleNoise, screenPos+(_particleNoiseDirection * 0.04 * _Time.y)).a);
						transmittance *= CalculateLightTransmittance(stepSize);
					}

					raymarchPos += rayDirection * stepSize; //Move position along ray
				}

				return float4(_lightColour.rgb * volLight, transmittance); //Volumeteric light stored as rgb and Transmittance stored in alpha channel
			}


#if defined(POINT) //Only compile for pointlights
			//Returns the length of the intersecting ray through a sphere - Changed for Front face culling - now uses distance to End of rayIntersectionLength
			float RaySphereIntersectionLength(float3 rayDirection, float distanceToIntersectionEnd)
			{
				return (distanceToIntersectionEnd - dot((_lightSourcePos.xyz - _WorldSpaceCameraPos), rayDirection)) * 2;
			}

#else //SPOT

			//Returns the angle (in radians) between two non-normalised vectors
			float AngleBetweenVectors(float3 vector1, float3 vector2)
			{
			//OLD Alternate tested methods to calculate angle between vectors
				//return acos(clamp(dot(normalize(vector1), normalize(vector2)), -1, 1));
				//return atan2(vector2.y - vector1.y, vector2.x - vector1.x);
				//return atan2(length(cross(vector1,vector2)), dot(vector1,vector2));
				//return acos(dot(vector1 / length(vector1), vector2 / length(vector2)));

				float dotResult = dot(normalize(vector1), normalize(vector2));
				if (dotResult < 0) //When the dot was below 0, the angle was giving weird results.
					dotResult *= -1;
				return acos(clamp(dotResult, -1, 1)); //Clamped to stop small errors as acos can throw weird results if even slightly out of the -1 to 1 range
			}

			//Returns the length of the intersecting ray through a cone
			float RayConeIntersectionLength(float3 rayDirection, float3 intersectionEndPos)
			{
				float3 lightOriginToEndVector = intersectionEndPos - _lightSourcePos;

				float distanceToCA_E = dot(lightOriginToEndVector, _lightSourceDirection);

				//When the intersection end is on the base of the Cone
				bool intersectionEndOnBase = distanceToCA_E >= (_lightSourceRange * 0.99); //0.99 bias added to catch any small calculation errors

				if(intersectionEndOnBase)
				{
					//Change from paper Notes: BC changed to CA_E to match other calculatiions as they turned out similar
					distanceToCA_E = _lightSourceRange;
				}

				float3 CA_E = _lightSourcePos + (_lightSourceDirection * distanceToCA_E); //CA_E is BC in some notes

				float3 CAM_E = _WorldSpaceCameraPos + (_lightSourceDirection * (dot((_WorldSpaceCameraPos - _lightSourcePos), _lightSourceDirection) - distanceToCA_E));

				float3 CAM_EToEndDirection = normalize(intersectionEndPos - CAM_E);

				float3 AC = CAM_E + (CAM_EToEndDirection * dot((CA_E - CAM_E), CAM_EToEndDirection));

				if(intersectionEndOnBase)
				{
					//TODO FIX calculation

					//WORKAROUND until calculation is fixed
					return -1; //Stops base rendering




					float3 ACToCA_EDistance = distance(AC, CA_E);
					float3 CE = AC + (CAM_EToEndDirection * ((_lightSourceRadius * _lightSourceRadius) - (ACToCA_EDistance * ACToCA_EDistance)));

					float3 O_AC = _lightSourcePos + (AC - CA_E);

					float3 CEAngle = AngleBetweenVectors((intersectionEndPos - CE), (O_AC - CE));
					float3 IEAngle = AngleBetweenVectors((intersectionEndPos - CE), rayDirection);
					float3 ISAngle = PI - (IEAngle + CEAngle); //180 converted to PI for radians

					//Normal usage - not working correctly TODO FIX
					return (distance(intersectionEndPos, CE)/sin(ISAngle)) * sin(CEAngle);
				}
				else
				{
					//REMOVED - No longer an issue after fixing angle calculations
					/*
					//Workaround for small intersection length problem. Returns -1 if the AC position is outside the cone(is past the intersection end)
					if(distance(CAM_E, AC) > distance(CAM_E, intersectionEndPos))
						return -1;
					*/

					//Spotlight to Intersection angle
					float SIAngle = AngleBetweenVectors(AC - _lightSourcePos.xyz, lightOriginToEndVector) * 2;

					//Angles are in radians //PI radians = 180 degrees
					return (distance(_lightSourcePos, intersectionEndPos) / sin(PI - (SIAngle + (AngleBetweenVectors(rayDirection, intersectionEndPos-_lightSourcePos))))) * sin(SIAngle);
				}


			}
#endif

			float4 frag (v2f i) : SV_Target
			{
				float3 worldPos = i.worldPos;
				//float worldPosDistance = i.worldPos.w; //Was more optimised but seemed to have some small error that threw calculations off
				float worldPosDistance = distance(_WorldSpaceCameraPos, worldPos);
				float3 rayDirection = i.viewDir;

				float depth;
				if(_calculationResolution == 1) //Full resolution
					depth = LinearEyeDepth(tex2Dproj(_CameraDepthTexture, i.screenPos)) * (_depthBias + 1); //Depth bias can be adjusted by user to compensate for inaccuracys in depth texture
				else if(_calculationResolution == 2) //Half resolution
					depth = LinearEyeDepth(tex2Dproj(_CameraDepthTextureHalfRes, i.screenPos)) * (_depthBias + 1); //Depth bias can be adjusted by user to compensate for inaccuracys in depth texture
				else //_calculationResolution == 4	 //Quarter resolution
					depth = LinearEyeDepth(tex2Dproj(_CameraDepthTextureQuarterRes, i.screenPos)) * (_depthBias + 1); //Depth bias can be adjusted by user to compensate for inaccuracys in depth texture


#if defined(POINT)
				float rayIntersectionLength = RaySphereIntersectionLength(rayDirection, worldPosDistance); //Point light
#else 	//SPOT
				float rayIntersectionLength = RayConeIntersectionLength(rayDirection, worldPos); //Spotlight
#endif

				//Calculate start position and distance of raymarch
				float3 raymarchStartPos;
				float raymarchDistance;
				if(_cameraInsideLightVolume == 1) //Inside light volume
				{
					raymarchStartPos = _WorldSpaceCameraPos;
					raymarchDistance = min(depth, worldPosDistance);
				}
				else
				{
					raymarchStartPos = worldPos - (rayDirection * rayIntersectionLength);

					raymarchDistance = min(rayIntersectionLength, depth-(worldPosDistance-rayIntersectionLength)); //distance = min(intersectionlength and distance from startpos to depth pos)


					if(raymarchDistance <= 0)
						return float4(0,0,0,1);
					else if(raymarchDistance > _maxRaymarchLength)
						raymarchDistance = _maxRaymarchLength;
				}

				float4 col = Raymarch(raymarchStartPos, rayDirection, raymarchDistance, i.screenPos.xy/i.screenPos.w);
				return col;
			}

			ENDCG
		}
	}
}
