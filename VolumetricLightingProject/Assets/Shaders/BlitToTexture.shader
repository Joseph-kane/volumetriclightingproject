﻿Shader "VolumetricLighting/BlitToTexture"
{
	Properties
	{
		_MainTex ("Texture", any) = "" {}
	}
	SubShader
	{
		Pass
		{
			ZWrite Off
			ZTest Always
			Blend One Zero
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			sampler2D _VolLight;
			uniform float4 _MainTex_ST;
			float4 _VolLight_TexelSize;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target
			{
				float4 col = tex2D(_MainTex, i.uv); //Colour of source pixel

//On some systems the UV position on textures has the y values starting at 0 at the top instead of the bottom
#if UNITY_UV_STARTS_AT_TOP
				if (_VolLight_TexelSize.y < 0)
					i.uv.y = 1 - i.uv.y;
#endif

				float4 volLight = tex2D(_VolLight, i.uv); //Volumetric lighting information

				if(volLight.a > 0) //Only effect pixels with volumetric light
				{
					col *= volLight.a; //Apply transimttance
					col.rgb += volLight.rgb; //Apply volumetric light colour
				}
				return col;
			}
			ENDCG
		}
	}
}
