Shader "VolumetricLighting/Blur"
{
	Properties
	{
        _MainTex("Texture", any) = "" {}
	}

	CGINCLUDE

	#include "UnityCG.cginc"

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	sampler2D _MainTex;

	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}

	//Returns the current pixel blurred
	float4 GetPixelBlurred(float2 blurDirection, float2 sourcePixelPos, float2 pixelSize, sampler2D depthTexture)
	{
		const float GaussianWeight[7] = {0.106595,	0.140367,	0.165569,	0.174938,	0.165569,	0.140367,	0.106595};  //Calculated using a gaussian calculator with: deviation=3 and kernel size of 7

		float2 pixelOffset = blurDirection * pixelSize;
		float2 samplePixelsPos[7] =
		{
			sourcePixelPos - (pixelOffset * 3),
			sourcePixelPos - (pixelOffset * 2),
			sourcePixelPos - pixelOffset,
			sourcePixelPos,
			sourcePixelPos + pixelOffset,
			sourcePixelPos + (pixelOffset * 2),
			sourcePixelPos + (pixelOffset * 3),
		};

		//Get depths
		float sampleDepths[7];
		for(int i=0; i<7; i++)
		{
			sampleDepths[i] = LinearEyeDepth(tex2D(depthTexture, samplePixelsPos[i]));
		}

		float weights[7];
		float weightsTotal = 0;

		for(int j=0; j<7; j++)
		{
			weights[j] = GaussianWeight[j] / (5 + abs(sampleDepths[3] - sampleDepths[j]));
			weightsTotal += weights[j];
		}

		//Normalize weights
		for(int k=0; k<7; k++)
		{
			weights[k] /= weightsTotal;
		}

		float4 col = float4(0,0,0,0);
		for(int l=0; l<7; l++)
		{
			col += tex2D(_MainTex, samplePixelsPos[l]) * weights[l];
		}

		return col;

	}
	ENDCG

	SubShader
	{
		//Pass 0: Full res horizontal blur
		Pass
		{
			Cull Off
			ZWrite Off
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTexture;
			float4 _CameraDepthTexture_TexelSize;

			float4 frag(v2f i) : SV_Target
			{
      	return GetPixelBlurred(float2(1,0), i.uv, _CameraDepthTexture_TexelSize.xy, _CameraDepthTexture);
			}

			ENDCG
		}

		//Pass 1: Full res vertical blur
		Pass
		{
			Cull Off
			ZWrite Off
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTexture;
			float4 _CameraDepthTexture_TexelSize;

			float4 frag(v2f i) : SV_Target
			{
      	return GetPixelBlurred(float2(0,1), i.uv, _CameraDepthTexture_TexelSize.xy, _CameraDepthTexture);
			}

			ENDCG
		}

		//Pass 2: Half res horizontal blur
		Pass
		{
			Cull Off
			ZWrite Off
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTextureHalfRes;
			float4 _CameraDepthTextureHalfRes_TexelSize;

			float4 frag(v2f i) : SV_Target
			{
      	return GetPixelBlurred(float2(1,0), i.uv, _CameraDepthTextureHalfRes_TexelSize.xy, _CameraDepthTextureHalfRes);
			}

			ENDCG
		}

		//Pass 3: Half res vertical blur
		Pass
		{
			Cull Off
			ZWrite Off
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTextureHalfRes;
			float4 _CameraDepthTextureHalfRes_TexelSize;

			float4 frag(v2f i) : SV_Target
			{
      	return GetPixelBlurred(float2(0,1), i.uv, _CameraDepthTextureHalfRes_TexelSize.xy, _CameraDepthTextureHalfRes);
			}

			ENDCG
		}

		//Pass 4: Quarter res horizontal blur
		Pass
		{
			Cull Off
			ZWrite Off
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTextureQuarterRes;
			float4 _CameraDepthTextureQuarterRes_TexelSize;

			float4 frag(v2f i) : SV_Target
			{
      	return GetPixelBlurred(float2(1,0), i.uv, _CameraDepthTextureQuarterRes_TexelSize.xy, _CameraDepthTextureQuarterRes);
			}

			ENDCG
		}

		//Pass 5: Quarter res vertical blur
		Pass
		{
			Cull Off
			ZWrite Off
			ZTest Always

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTextureQuarterRes;
			float4 _CameraDepthTextureQuarterRes_TexelSize;

			float4 frag(v2f i) : SV_Target
			{
      	return GetPixelBlurred(float2(0,1), i.uv, _CameraDepthTextureQuarterRes_TexelSize.xy, _CameraDepthTextureQuarterRes);
			}

			ENDCG
		}

	}
}
