﻿Shader "VolumetricLighting/BilateralUpsample"
{
	Properties
	{
		_MainTex ("Texture", any) = "" {}
	}

	CGINCLUDE

	#include "UnityCG.cginc"

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float2 uv : TEXCOORD0;
		float4 vertex : SV_POSITION;
	};

	sampler2D _MainTex; //The low resolution volumetric light buffer

	sampler2D _CameraDepthTexture;
	float4 _CameraDepthTexture_TexelSize;


	v2f vert (appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}

	float4 GetBilateralUpsample(float2 uv, sampler2D lowResDepthTexture)
	{
		//1:  1/16, 3/16, 3/16, 9/16
		//2:  3/16, 1/16, 9/16, 3/16
		//3:  3/16, 9/16, 1/16, 3/16
		//4:  9/16, 3/16, 3/16, 1/16
		const float4 bilinearWeights[4] =
		{
			float4(0.0625, 0.1875, 0.1875, 0.5625),
			float4(0.1875, 0.0625, 0.5625, 0.1875),
			float4(0.0625, 0.5625, 0.0625, 0.5625),
			float4(0.5625, 0.1875, 0.1875, 0.0625)
		};

		const float2 offsetFirstLowResPixel[4] = {float2(-2,2), float2(-1,-2), float2(-2,-1), float2(-1,-1)}; //Uv at top left of pixel. First pixel in 2x2 grid

		int highResPixelNum = fmod(uv.x * _CameraDepthTexture_TexelSize.z, 2) + (2 * fmod(uv.y * _CameraDepthTexture_TexelSize.w, 2)); //0->3 value representing pixel numbers 1->4 of high res pixel on a 2x2 Grid

		float2 lowResPixelPos[4];
		lowResPixelPos[0] = uv + (offsetFirstLowResPixel[highResPixelNum] * _CameraDepthTexture_TexelSize.xy); //Position of nearest low res pixel to the top left
		lowResPixelPos[1] = lowResPixelPos[0] + float2(_CameraDepthTexture_TexelSize.x, 0); //Position of nearest low res pixel to the top right
		lowResPixelPos[2] = lowResPixelPos[0] + float2(0, _CameraDepthTexture_TexelSize.y); //Position of nearest low res pixel to the bottom left
		lowResPixelPos[3] = lowResPixelPos[1] + float2(0, _CameraDepthTexture_TexelSize.y); //Position of nearest low res pixel to the bottom right

		//Get depths
		float highResDepth = LinearEyeDepth(tex2D(_CameraDepthTexture, uv));

		//Get lowResDepth
		float lowResDepth[4];
		for(int m=0; m<4; m++)
		{
			lowResDepth[m] = LinearEyeDepth(tex2D(lowResDepthTexture, lowResPixelPos[m]));
	 	}

		float weights[4];
		float weightsTotal = 0;

		for(int j=0; j<4; j++)
		{
			weights[j] = bilinearWeights[highResPixelNum] / (1 + abs(highResDepth - lowResDepth[j]));
			weightsTotal += weights[j];
		}

		//Normalize weights
		for(int k=0; k<4; k++)
		{
			weights[k] /= weightsTotal;
		}

		float4 col = float4(0,0,0,0);
		for(int l=0; l<4; l++)
		{
			col += tex2D(_MainTex, lowResPixelPos[l]) * weights[l];
		}

		return col;
	}

	ENDCG

	SubShader
	{
		//Pass 0: Half Res Upsample
		Pass
		{
			ZWrite Off
			ZTest Always
			Blend One One
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTextureHalfRes;

			float4 frag (v2f i) : SV_Target
			{
				return GetBilateralUpsample(i.uv, _CameraDepthTextureHalfRes);
			}

			ENDCG
		}

		//Pass 1: Quarter Res Upsample
		Pass
		{
			ZWrite Off
			ZTest Always
			Blend One One
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTextureQuarterRes;

			float4 frag (v2f i) : SV_Target
			{
				return GetBilateralUpsample(i.uv, _CameraDepthTextureQuarterRes);
			}

			ENDCG
		}
	}
}
