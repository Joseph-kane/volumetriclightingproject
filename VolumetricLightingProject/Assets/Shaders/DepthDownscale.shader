﻿Shader "VolumetricLighting/DepthDownscale"
{
	//Functions shared between passes
	CGINCLUDE

	#include "UnityCG.cginc"

	struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
	};

	struct v2f
	{
		float4 vertex : SV_POSITION;
		float2 uv : TEXCOORD0;
	};

	sampler2D _CameraDepthTexture;
	float4 _CameraDepthTexture_TexelSize;

	v2f vert (appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
		return o;
	}

	float GetDownscaledDepth(float2 uv, float2 offsetSize, sampler2D depthTex)
	{
		float2 sample1uv = float2(uv + float2(-1,-1)*offsetSize);
		float2 sample2uv = float2(uv + float2(-1,1)*offsetSize);
		float2 sample3uv = float2(uv + float2(1,-1)*offsetSize);
		float2 sample4uv = float2(uv + float2(1,1)*offsetSize);

		float sample1 = tex2D(depthTex, sample1uv);
		float sample2 = tex2D(depthTex, sample2uv);
		float sample3 = tex2D(depthTex, sample3uv);
		float sample4 = tex2D(depthTex, sample4uv);

		return min(sample1, min(sample2, min(sample3, sample4))); //Gets minimum value from samples
	}
	ENDCG

	//Indiviual passes for Half and Quarter depth downscaling
	SubShader
	{
		//Pass 0: Half resolution downscale
		Pass
		{
			ZTest Always Cull Off ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			float frag (v2f i) : SV_Target
			{
				float2 offsetSize = 0.5 * _CameraDepthTexture_TexelSize.xy;
				return GetDownscaledDepth(i.uv, offsetSize, _CameraDepthTexture);
			}
			ENDCG
		}

		//Pass 1: Quarter resolution downscale
		Pass
		{
			ZTest Always Cull Off ZWrite Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			sampler2D _CameraDepthTextureHalfRes;
			float4 _CameraDepthTextureHalfRes_TexelSize;

			float frag (v2f i) : SV_Target
			{
				float2 offsetSize = 0.5 * _CameraDepthTextureHalfRes_TexelSize.xy;
				return GetDownscaledDepth(i.uv, offsetSize, _CameraDepthTextureHalfRes);
			}
			ENDCG
		}
	}
}
